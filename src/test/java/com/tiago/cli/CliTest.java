package com.tiago.cli;

import java.io.File;

import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CliTest {

  @Rule
  public ExpectedException expectedEx = ExpectedException.none();
  
  private static final ClassLoader CLASS_LOADER = CliTest.class.getClassLoader();
  
  private static final File TEST_FILE = new File(CLASS_LOADER.getResource("testFile.txt").getFile());
  
  private static final String TEST_FILE_PATH = TEST_FILE.getAbsolutePath();
  
  @Test
  public void whenUnrecognizedOption_thenThrowUnrecognizedOptionException() throws Exception {
    expectedEx.expect(UnrecognizedOptionException.class);
    expectedEx.expectMessage("Unrecognized option: -orange");
    
    String[] arguments = { "-orange", "5" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenAccesslogOptionIsMissing_thenThrowMissingOptionException() throws Exception {
    expectedEx.expect(MissingOptionException.class);
    expectedEx.expectMessage("Missing required option: accessLog");
    
    String[] arguments = { "-startDate", "2017-01-01.01:00:00", "-duration", "daily", "-threshold", "2" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenAccesslogOptionRefersToInvalidFile_thenThrowIllegalArgumentException() throws Exception {
    String invalidFilePath = "/invalid/file/path";
    
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("File \"" + invalidFilePath + "\" is not a valid file");
    
    String[] arguments = { "-accessLog", invalidFilePath, "-startDate", "2017-01-01.01:00:00", "-duration", "daily", "-threshold", "2" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenStartDateOptionIsMissing_thenThrowMissingOptionException() throws Exception {
    expectedEx.expect(MissingOptionException.class);
    expectedEx.expectMessage("Missing required option: startDate");
    
    String[] arguments = { "-accessLog", "/path/to/file", "-duration", "daily", "-threshold", "2" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenStartDateOptionIsInvalidDate_thenThrowIllegalArgumentException() throws Exception {
    String invalidDate = "2019-01-a";
    
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("Date \"" + invalidDate + "\" is not a valid date");
    
    String[] arguments = { "-accessLog", TEST_FILE_PATH, "-startDate", invalidDate, "-duration", "daily", "-threshold", "2" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenDurationOptionIsMissing_thenThrowMissingOptionException() throws Exception {
    expectedEx.expect(MissingOptionException.class);
    expectedEx.expectMessage("Missing required option: duration");
    
    String[] arguments = { "-accessLog", TEST_FILE_PATH, "-startDate", "2017-01-01.01:00:00", "-threshold", "2" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenDurationOptionIsInvalidDuration_thenThrowIllegalArgumentException() throws Exception {
    String invalidDuration = "dailyyy";
    
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("Duration \"" + invalidDuration + "\" is not a valid duration");
    
    String[] arguments = { "-accessLog", TEST_FILE_PATH, "-startDate", "2017-01-01.01:00:00", "-duration", invalidDuration, "-threshold", "2" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenThresholdOptionIsMissing_thenThrowMissingOptionException() throws Exception {
    expectedEx.expect(MissingOptionException.class);
    expectedEx.expectMessage("Missing required option: threshold");
    
    String[] arguments = { "-accessLog", TEST_FILE_PATH, "-startDate", "2017-01-01.01:00:00", "-duration", "daily" };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenThresholdOptionIsNegativeInteger_thenThrowIllegalArgumentException() throws Exception {
    String negativeThreshold = "-1";
    
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("Threshold must be a positive integer");
    
    String[] arguments = { "-accessLog", TEST_FILE_PATH, "-startDate", "2017-01-01.01:00:00", "-duration", "daily", "-threshold", negativeThreshold };
    
    new Cli(arguments).parse();
  }
  
  @Test
  public void whenThresholdOptionIsInvalidThreshold_thenThrowIllegalArgumentException() throws Exception {
    String invalidThreshold = "a";
    
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("Threshold \"" + invalidThreshold + "\" is not a valid integer");
    
    String[] arguments = { "-accessLog", TEST_FILE_PATH, "-startDate", "2017-01-01.01:00:00", "-duration", "daily", "-threshold", invalidThreshold };
    
    new Cli(arguments).parse();
  }
}
