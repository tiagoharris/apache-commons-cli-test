package com.tiago;

import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tiago.cli.Cli;

/**
 * This is the main spring boot application class.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@SpringBootApplication
public class ApacheCommonsCliTestApplication implements CommandLineRunner {
  
  private static Logger LOG = LoggerFactory.getLogger(ApacheCommonsCliTestApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ApacheCommonsCliTestApplication.class, args);
	}

  @Override
  public void run(String... args) {
    LOG.info("beginning execution...");
    
    Cli cli = new Cli(args);
    
    try {
      cli.parse();
    } catch (ParseException e) {
      LOG.error(e.getMessage());
      cli.printHelp();
    } catch (IllegalArgumentException e) {
      LOG.error(e.getMessage());
    }
    
    LOG.info("... end");
  }
}

